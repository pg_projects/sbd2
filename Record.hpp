#pragma once
#include <iostream>
#include <cstring>
#include <cmath>
#include <cstdint>
#include <iomanip>

class Record
{
    static const char numbers = 5;
public:
    void setFactorNumber()
    {
        uint_fast32_t sum = 1;
		factorNumber = 0;

        for(int i=0 ;i < numbers; i++)
            sum *= values[i];

        for(int i=1;i<=sqrt(sum); i++)
        {
            if(sum % i ==0)
                factorNumber+=2;
        }
    }
public:
    int_fast32_t values[numbers];
	int key;
    uint_fast32_t factorNumber = 0;
	static int readDisk, writeDisk;
    Record()
    {}

    Record(const int_fast32_t values[numbers])
    {
        memcpy(this->values, values, numbers*sizeof(int_fast32_t));
//        setFactorNumber();
    }

	friend inline bool operator== (const Record& lhs, const Record& rhs) { return lhs.key == rhs.key; }
	friend inline bool operator< (const Record& lhs, const Record& rhs) { return lhs.key < rhs.key; } //return lhs.factorNumber < rhs.factorNumber; }
    friend inline bool operator> (const Record& lhs, const Record& rhs) { return rhs < lhs; }
    friend inline bool operator<=(const Record& lhs, const Record& rhs) { return !(lhs > rhs); }
    friend inline bool operator>=(const Record& lhs, const Record& rhs) { return !(lhs < rhs); }

	void print()
	{
		for (int i = 0; i < 5; i++)
			printf("%d ", values[i]);

//		setFactorNumber();
	}
};
int Record::writeDisk = 0;
int Record::readDisk = 0;
//B 4096 bytes
//R