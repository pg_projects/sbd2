#include "BTree.hpp"
#include <iostream>
#include <algorithm>

#define MAX_KEYS 2*t
BTreeNode::BTreeNode(int t, bool leaf):
	t(t),
	leaf(leaf),
	n(0)
{
	keys.resize(MAX_KEYS + 1); // 1 wiecej klucz dla dzielenia
	C.resize(MAX_KEYS+1);

}

void BTreeNode::traverse()
{
	int i;
	// idziemy po kluczach i pierwsze n dzieci
	for (i = 0; i < n; i++)
	{
		// jesli nie lisc, idziemy po dzieciach
		if (leaf == false)
			C[i]->traverse();

		cout << " " << keys[i];
	}

	// przechodzimy przez ostatnie dziecko
	if (leaf == false)
		C[i]->traverse();
}

BTreeNode *BTreeNode::search(int k)
{
	int i = 0;

	while (i < n && k > keys[i]) // szukamy klucza wi�kszego lub r�wnego szukanemu
		i++;

	if (keys[i] == k)
		return this;
	else if (leaf == true) // zakoncz jesli jest lisciem
		return NULL;
	else
		return C[i]->search(k);
}

void BTree::insert(int k)
{
	if (root == NULL) // drzewo jest puste, wstawiamy korzen
	{
		root = new BTreeNode(t, true);
		root->keys[0] = k;
		root->n = 1;
	}
	else // korzen istnieje
	{
		if (root->n == 2 * t)
		{
			BTreeNode *s = new BTreeNode(t, false);

			s->C[0] = root;
			root->keys[2 * t] = k; // wstawiamy niewidzialny element
			std::sort(root->keys.begin(), root->keys.end()); // sortujemy strone

			s->splitChild(0, root);

			int i = 0;

			if (s->keys[0] < k)
				i++;
//			s->C[i]->insertNonFull(k);
			root->keys[2*t]= -1;
			root = s;
		}
		else  // korzen nie pelny, wstawiamy klucz 
			root->insertNonFull(k);
	}
}

void BTreeNode::insertNonFull(int k)
{
	int i = n - 1;

	if (leaf == true)
	{
		while (i >= 0 && keys[i] > k) // szukamy miejsca dla klucza i przesuwamy reszt�
		{
			keys[i + 1] = keys[i];
			i--;
		}

		//wstawiamy
		keys[i + 1] = k;
		n = n + 1;
	}
	else
	{
		while (i >= 0 && keys[i] > k) // szukamy miejsca dla klucza i przesuwamy reszt�
			i--;

		if (C[i + 1]->n == 2 * t) // dziecko jest pelne
		{
			//this->insertNonFull(k);
			C[i + 1]->keys[2 * t] = k;
			splitChild(i + 1, C[i + 1]);
			return;
			//decydujemy do ktorego dziecka wstawic nowy klucz
			if (keys[i + 1] < k)
				i++;
		}
		C[i + 1]->insertNonFull(k);
	}
}

void BTreeNode::splitChild(int i, BTreeNode *y) /// y - strona przepelniona //elem 5 na gore
{
	// tworzymy node kt�r� b�dzie miala t-1 kluczy
	BTreeNode *z = new BTreeNode(y->t, y->leaf);
	z->n = t;

	// kopiujemy t-1 kluczy y do z
	for (int j = 0; j < t; j++)
		z->keys[j] = y->keys[j + t+1];


	// kopiujemy t ostatnich kluczy y do z
	if (y->leaf == false)
	{
		for (int j = 0; j < t; j++)
			z->C[j] = y->C[j + t+1];
	}

	//  zmieniejszamy liczby kluczy w y
	y->n = t;

	// robimy miejsce dla nowego dziecka, przesuwajac wszystko w prawo
	for (int j = n; j >= i + 1; j--)
		C[j + 1] = C[j];

	// Link the new child to this node 
	C[i + 1] = z;

	// Znajdujemy nowy klucz 
	// robimy miejsce dla naszego kluca, przesuwamy wszystkie klucze wieksze od k w prawo
	for (int j = n - 1; j >= i; j--)
		keys[j + 1] = keys[j];

	// kopiujemy srodkowy klucz
	keys[i] = y->keys[t];
	n = n + 1;
}