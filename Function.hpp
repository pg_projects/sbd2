#include<vector>
#include<string>
#include<random>

template<typename T>
std::string generateRandomOperations(const std::vector<T>& collection, const int length)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dst(0, collection.size() - 1);

	std::string result;
	result.reserve(length);

	for (int i = 0; i < length; i++)
		result += collection[dst(mt)];

	return result;
}

void interprete()
{
	int del = 0, add = 0;

	int ar1[] = { 10, 100, 1000, 10000, 100000 };

	for (auto i : ar1)
	{
		std::ifstream file("docs\\examples\\outCommand" + to_string(i) + ".txt");

		while (file.peek() != std::ifstream::traits_type::eof())
		{
			char t;
			file >> t;

			if (t == 'D' && del > 0)
				del--;
			else if (t == 'A')
				del++;
		}
		file.close();
		std::cout << del << std::endl;
		del = 0;
	}
}

enum COMMAND
{
	Add = 1,
	Update,
	Delete,
	Print_Index,
	Print_File,
	End = 0
};

enum FILE_TYPE
{
	Index,
	Database
};

void printMenu()
{
	std::cout << "[1] Add \n[2] Update \n";
	std::cout << "[3] Remove \n[4] Print Index \n";
	std::cout << "[5] Print File \n[0] End \n";
}

void addRecord(const BTree& tree)
{
	//Get User Input

	int tab[5] = { -1 };

	std::cout << "Prosze wprowadzic 5 liczb calkowitych \n";

	for (int i = 0; i < 5; i++)
		std::cin >> tab[i];

	Record record(tab);
	

}

void updateRecord(const BTree& tree)
{
	//Get User Input

	int tab[5] = { -1 };
	std::size_t key;

	std::cout << "Prosze wprowadzic klucz rekordu do aktualizacji \n";
	std::cin >> key;

	std::cout << "Prosze wprowadzic 5 liczb calkowitych \n";

	for (int i = 0; i < 5; i++)
		std::cin >> tab[i];

	Record record(tab);
}

void removeRecord(const BTree& tree)
{
	//Get User Input

	int tab[5] = { -1 };
	std::size_t key;

	std::cout << "Prosze wprowadzic klucz rekordu do usuniecia \n";
	std::cin >> key;

}

void printFile(const FILE_TYPE type)
{
	std::ifstream file;

	switch (type)
	{
	case FILE_TYPE::Database:
	{
		file.open(DATABASE_PATH, std::ifstream::binary);
		break;
	}

	case FILE_TYPE::Index:
	{
		file.open(INDEX_PATH, std::ifstream::binary);
		break;
	}
	}

	std::cout << file.rdbuf();
	file.close();
}

char detectUserInput(const BTree& tree)
{
	char command = -1;

		std::cin >> command;

		switch (command)
		{
		case COMMAND::Add:
			addRecord(tree);
			break;
		case COMMAND::Update:
			updateRecord(tree);
			break;
		case COMMAND::Delete:
			removeRecord(tree);
			break;
		case COMMAND::Print_Index:
			printFile(FILE_TYPE::Index);
			break;
		case COMMAND::Print_File:
			printFile(FILE_TYPE::Database);
			break;
		case COMMAND::End:
			break;
		}

	return command;
}
