#pragma once
#include <iostream>
#include <vector>
using namespace std;

class BTreeNode
{
	vector<int> keys;
	int t;
	vector<BTreeNode*> C;
	int n;
	bool leaf;
public:
	BTreeNode(int _t, bool _leaf);   // Constructor 

	void insertNonFull(int k);
	void splitChild(int i, BTreeNode *y); // Tylko dla pelnych dzieci
	void traverse();
	BTreeNode *search(int k);   // returns NULL if k is not present. 

	friend class BTree;
	friend class BTreePrinter;
};

class BTree
{
	BTreeNode *root;
	int t;  // Minimum degree 
public:
	BTree(int _t)
	{
		root = NULL;  t = _t;
	}

	void traverse()
	{
		if (root != NULL) root->traverse();
	}

	BTreeNode* search(int k)
	{
		return (root == NULL) ? NULL : root->search(k);
	}

	void insert(int k);
	friend class BTreePrinter;
};